public class Bear{ 
	
	public String name;
	public int age;
	public double weight;
	public boolean isAgressive;
	
	/*
	This method takes no parameters. 
	Depending on certain criterias, it will display different inputs. 
	If the bear's weight >= and age is >= it will display that he isn't 
	capable of doing tricks. If the bear is agressive it will display the text indicating that 
	the bear is not happy. Otherwise it will print that the bear did the trick.
	*/
	public void canDoTricks(){
		if(this.weight >= 200 || this.age >= 15){
			System.out.println(this.name + " doesn't have capabilities to do such things");
		} else if (this.isAgressive == true){ 
			System.out.println(this.name + " is definetely not happy with your actions");
		} else { 
			System.out.println(this.name + " did the coolest trick that bear can do");
		}
		
	}
	
	/*
	This method takes no parameters. 
	Depending on the value of boolean isAgressive, it will print the given name
	of the bear and his reaction.
	*/
	public void canPet(){
		if(this.isAgressive == false){
			System.out.println(this.name + " did let you pet him");
		} else { 
			System.out.println(this.name +" wasn't happy with your presence");
		}
	}
	
}

