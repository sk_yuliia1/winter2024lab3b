public class VirtualPetApp{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("~~Welcome to VirtualPetApp!~~");
		System.out.println("~~Bear edition~~");
		Bear[] sloth = new Bear[4];
		
		/*
		In this loop, the program asks user to fill the array with required fields,
		asking the user to enter a String name, an int age,
		a double weight and a boolean asking him if he is agressive or not
		*/
		for(int i = 0; i<sloth.length; i++){
			sloth[i] = new Bear();
			System.out.println("Enter a name to a bear: ");
			sloth[i].name = reader.nextLine();
			System.out.println("Enter an age: ");
			sloth[i].age = Integer.parseInt(reader.nextLine()); 
			System.out.println("Enter a weight: ");
			sloth[i].weight = Double.parseDouble(reader.nextLine());
			System.out.println("Is bear agressive? (true or false) ");
			sloth[i].isAgressive = Boolean.parseBoolean(reader.nextLine());
		}
		
		// This block of code simply prints all the fields of the last bear in the array
		System.out.println(sloth[sloth.length - 1].name);
		System.out.println(sloth[sloth.length - 1].age);
		System.out.println(sloth[sloth.length - 1].weight);
		System.out.println(sloth[sloth.length - 1].isAgressive);
		
		//By this, we invoke the methods from class Bear, using the first value of bear in the array
		sloth[0].canPet();
		sloth[0].canDoTricks();
	}
}

